import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardAlphaComponent } from './pages/dashboard-alpha/dashboard-alpha.component';
import { BarraComponent } from './pages/barra/barra.component';
import { GraficoBarraComponent } from './pages/grafico-barra/grafico-barra.component';
import { MaterializeGridComponent } from './pages/materialize-grid/materialize-grid.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardAlphaComponent,
    BarraComponent,
    GraficoBarraComponent,
    MaterializeGridComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
