import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAlphaComponent } from './dashboard-alpha.component';

describe('DashboardAlphaComponent', () => {
  let component: DashboardAlphaComponent;
  let fixture: ComponentFixture<DashboardAlphaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAlphaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAlphaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
