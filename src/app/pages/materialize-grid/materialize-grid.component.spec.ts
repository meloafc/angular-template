import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterializeGridComponent } from './materialize-grid.component';

describe('MaterializeGridComponent', () => {
  let component: MaterializeGridComponent;
  let fixture: ComponentFixture<MaterializeGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterializeGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterializeGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
